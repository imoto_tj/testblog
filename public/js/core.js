$(document).on("click", "#like",function (e){
            var newsid = $(this).attr('data-id');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                e.preventDefault();
                $.ajax({
                    url: "/like/"+newsid,
                    type: 'GET',
                    success: function (data) {
                        //console.log(data);
                        document.getElementById('news'+newsid).innerHTML = '<i class="bi bi-heart-fill lead"></i> '+data;
                    }
                })
        });

function view(newsid) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: "/view/"+newsid,
        type: 'GET',
        success: function (data) {
            var old = document.getElementById(newsid).innerHTML;
            old++;
            document.getElementById(newsid).innerHTML = ''+ old;
        }
    })
}

function trig(e) {
    setTimeout(function(){ view(e);}, 5000);
}

$(function(){
    $('div[onload]').trigger('onload');
});

$('#comments').on('submit', function(e) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    e.preventDefault();
    $.ajax({
        url: "/api/addcomment",
        type: 'POST',
        data: $('#comments').serialize(),
        success: function (data) {
            $('#err').fadeOut();
            document.getElementById('comments').innerHTML = '<div class="alert alert-success" role="alert">Ваше сообщение успешно отправлено</div>';
        },
        error: function (err) {
            if (err.status == 422) {
                //console.log(err.responseJSON)
                document.getElementById('err').innerHTML = '<div class="alert alert-danger" role="alert">'+err.responseJSON.message+'</div>';

            }
        }

    })


});
