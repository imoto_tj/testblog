<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $table='article';

    public function tags(){
        return $this->hasMany(Tag::class, 'article_id');
    }

    public function views(){
        return $this->hasMany(View::class, 'article_id');
    }
}
