<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $table='comment';

    protected $fillable=['newsid','subject','message'];

    public function articles()
    {
        return $this->belongsTo(Article::class,'id');
    }
}
