<?php

namespace App\Jobs;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CommentAddJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $article;
    private $subj;
    private $message;

    /**
     * Create a new job instance.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->article =  $request->get('newsid');
        $this->subj = $request->get('subject');
        $this->message = $request->get('message');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //sleep(10);
        $com = new Comment();
        $com->article_id = $this->article;
        $com->subject=$this->subj;
        $com->message=$this->message;
        $com->save();
    }
}
