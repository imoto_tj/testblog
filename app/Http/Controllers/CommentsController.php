<?php

namespace App\Http\Controllers;

use App\Jobs\CommentAddJob;
use Illuminate\Http\Request;


class CommentsController extends Controller
{

    public function comment(Request $request){
        $request->validate([
                'subject' => 'required|max:255',
                'message' => 'required',
                'newsid' => 'required|numeric',
            ]
        );

        CommentAddJob::dispatch($request)->delay(now()->addMinutes(10));
    }
}
