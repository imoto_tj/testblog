<?php

namespace App\Http\Controllers;

use App\Models\Like;
use Illuminate\Http\Request;

class LikesController extends Controller
{
    public function like($id){
        $data = Like::where('article_id',$id)->first();
        if(!$data){
            $data = new Like();
            $data->article_id = $id;
            $data->value = 1;
            $data->save();
            return 1;
        }
        $data->increment('value');
        return $data->value;
    }
}
