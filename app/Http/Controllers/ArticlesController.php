<?php

namespace App\Http\Controllers;

use App\Models\Article;

class ArticlesController extends Controller
{
    public function index(){
        $data = Article::orderbydesc('id')->limit(6)->get();
        return view('main',compact('data'));
    }

    public function cat(){
        $data = Article::paginate(5);
        return view('articles',compact('data'));
    }

    public function article($id){
      $item = Article::find($id);
        return view('article',compact('item'));
    }

}
