<?php

namespace App\Http\Controllers;

use App\Jobs\ViewAddJob;

class ViewsController extends Controller
{
   public function view($id){
       ViewAddJob::dispatch($id);
    }
}
