$(document).ready(function(){
        $("#like").click(function(e){
            var newsid = $(this).attr('data-id');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                e.preventDefault();
                $.ajax({
                    url: "/like/"+newsid,
                    type: 'GET',
                    success: function (data) {
                        console.log(data);
                        document.querySelectorAll("[data-id='"+newsid+"']").html('<i class="bi bi-heart-fill lead"></i> '+data);
                    }
                })
        });
});
