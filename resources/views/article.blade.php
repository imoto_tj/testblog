@extends('layout')
@section('content')
<div class="row justify-content-md-center">
    <div class="col-sm-10">
        <div class="card">

            <div class="card-body">
                <div class="card-title">
                    <h2 class="font-weight-bold">{{$item->title}}</h2>
                </div>
                <div class="row ml-1" onload="trig({{$item->id}})">
                    <button id="like" type="button" class="btn float-right" data-id="{{ $item->id }}"><div id="news{{$item->id}}"><i class="bi bi-heart lead"></i> </div></button>
                    @forelse($item->views as $val)
                        <i class="bi bi-eye lead" style="padding: 0.375rem 0.75rem;" id="{{$item->id}}">{{ $val->value }}</i>
                    @empty
                        <i class="bi bi-eye lead" style="padding: 0.375rem 0.75rem;" id="{{$item->id}}">0</i>
                    @endforelse
                </div>
                <div class="row ml-1 mt-3">
                    @foreach($item->tags as $tag)
                    <span class="badge badge-secondary mr-1 larger">{{$tag->name}}</span>
                    @endforeach
                </div>
                <div class="card-text mt-2">
                    {{$item->desc}}
                </div>
                <h4 class="mt-2 font-weight-bold">Комментарий<a class="anchorjs-link "></a></h4>
                <div id="err"></div>
                <form id="comments">
                    @csrf
                    <div class="mb-3 form-group">
                        <label for="exampleFormControlInput1" class="form-label">Тема сообщения</label>
                        <input type="text" name="subject" class="form-control" id="exampleFormControlInput1" placeholder="Тема">
                        <input type="hidden" value="{{$item->id}}" name="newsid">
                    </div>
                    <div class="mb-3 form-group">
                        <label for="exampleFormControlTextarea1" class="form-label">Текст сообщения</label>
                        <textarea class="form-control" name="message" placeholder="Сообщение" rows="4"></textarea>
                    </div>
                    <button class="btn btn-primary float-right" type="submit">Отправить</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
