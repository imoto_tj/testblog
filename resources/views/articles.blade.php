@extends('layout')
@section('content')

    <div class="row">
        <div class="col-sm-2">
                @foreach( $data as $tag)

                    @foreach($tag['tags'] as $tag_item)
                       <span class="larger badge badge-secondary">
                        <a class=" text-white" href="/tags/{{ $tag_item->name }}">{{ $tag_item->name }}</a>
                       </span>
                    @endforeach

                @endforeach

        </div>
        <div class="col-sm-10">
            @foreach($data as $item)
                <div class="card mb-3">
                    <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg"   preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"></rect><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text></svg>
                    <div class="card-body">
                        <h5 class="card-title"><a href="/article/{{$item->id}}"> {{ $item->title }}</a></h5>
                        <p class="card-text">{{\Illuminate\Support\Str::of($item->desc)->limit('100','...')}}</p>
                        @forelse($item->views as $val)
                            <i class="bi bi-eye lead"></i> <small class="text-muted">{{ $val->value }}</small>
                        @empty
                            <i class="bi bi-eye lead"></i> <small class="text-muted">0</small>
                        @endforelse
                        <button id="like" type="button" class="btn btn-link float-right" data-id="{{ $item->id }}"><div id="news{{$item->id}}"><i class="bi bi-heart lead"></i></div></button>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="row justify-content-md-center">
        {{ $data->links('pagination::bootstrap-4') }}
    </div>

    @endsection
