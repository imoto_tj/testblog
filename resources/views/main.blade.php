@extends('layout')
@section('content')

    <div class="pricing-header p-3 pb-md-4 mx-auto text-center">
        <h1 class="display-4 fw-normal">Test</h1>
        <p class="fs-5 text-muted">Quickly build an effective pricing table for your potential customers with this Bootstrap example.</p>
    </div>


    <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
        @foreach($data as $item)

       <div class="col mb-2">
            <div class="card shadow-sm">
                <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"></rect><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text></svg>

                <div class="card-body">
                    <div class="card-title"><a href="/article/{{$item->id}}"><h2>{{$item->title}}</h2></a></div>
                    <p class="card-text">{{\Illuminate\Support\Str::of($item->desc)->limit('100','...')}}</p>
                    @forelse($item->views as $val)
                        <i class="bi bi-eye lead"></i> <small class="text-muted">{{ $val->value }}</small>
                    @empty
                        <i class="bi bi-eye lead"></i> <small class="text-muted">0</small>
                    @endforelse
                    <button id="like" type="button" class="btn btn-link float-right" data-id="{{ $item->id }}"><div id="news{{$item->id}}"><i class="bi bi-heart lead"></i></div></button>
                </div>
            </div>
        </div>
            @endforeach
    </div>
@endsection
