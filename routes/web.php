<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ArticlesController@index');
Route::get('/articles','ArticlesController@cat');
Route::get('/article/{slug}','ArticlesController@article');

//calls
Route::get('/like/{id}','LikesController@like');
Route::get('/view/{id}','ViewsController@view');



